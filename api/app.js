//Require express for url requests
const express = require("express");

//Set app as the constant for express
const app = express();

//Require router.js for routes
const router = require("./router");

//Require controller.js for controller methods
const user = require("./controller");

//Set the default port. process.env.port can be found in docker-compose.yml
const PORT = process.env.port || 80;

//Allow JSON
app.use(express.json());

//Allow URL Encoding
app.use(
  express.urlencoded({
    extended: true,
  })
);

//Set one router that will handle api requests
app.use("/api", router);

//Run the app to a specified port
app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});