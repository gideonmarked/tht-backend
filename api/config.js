const config = {
  db: {
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: 'root',
    password: '1234',
    database: 'tht',
    port: 3306
  },
  listPerPage: 10,
};
module.exports = config;