const express = require('express');
const router = express.Router();
const users = require("./controller");

//Create middleware to test the api key validation
const checkApiKey = function (req, res, next) {
  const apiKey = req.header('x-api-key');
  console.log(apiKey + "!==" + process.env.API_KEY);
  if( apiKey !== process.env.API_KEY ) {
    res.send(400, 'missing authorization header');
  } else next()
}

//Returns a message
router.get('/', (req, res) => {
  res.json({ message: "THT Simple API is Working" });
});

//GET request for getting the list of user names. The controller method findAllNames is called from the controller.js
//This requires and API key to be able to access
router.get('/users/names', checkApiKey, users.findAllNames);

//GET request for getting a user information identified by <integer>:id value. The controller method findOne is called from the controller.js
router.get("/users/:id", checkApiKey, users.findOne);

//POST request for creating one user from a form submission
//This requires and API key to be able to access
router.post("/users", users.create);

module.exports = router;