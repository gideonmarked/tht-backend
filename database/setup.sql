/***CREATING ALL TABLES*/
CREATE TABLE users (
  StudentId   INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  FullName    VARCHAR(255)                    NULL,
  Email        VARCHAR(255)                   NULL
)
  ENGINE = INNODB;


INSERT INTO users (StudentId,FullName, Email)
VALUES (1, 'John Doe', 'foo1@foo.com'),
(2, 'Jane Doe', 'foo2@foo.com'),
(3, 'Mark Doe', 'foo3@foo.com');

DROP PROCEDURE IF EXISTS sp_GetStudent;
DELIMITER //
CREATE PROCEDURE sp_GetStudent()
  BEGIN
    SELECT * FROM users;
  END //
DELIMITER ;
/**Drop StoreProcedure**/
CALL sp_GetStudent();